// ==UserScript==
// @name         CoOpTimus - Enhanced UX
// @namespace    https://bitbucket.org/antonolsson91/cooptimus-enhanced-ux/
// @version      1.1.0
// @tags         co-optimus.com, cooptimus, switch games, coop, games, coop games, cooperation, couch games
// @description  Improves the user experience for co-optimus.com visitors by: |1) adding prices of the games listed |2) making the list sortable with modern tools |3) and more?!
// @author       Anton Olsson
// @authoremail  eagleenterprises08+cooptimus@gmail.com
// @license      GPL version 3 or any later version; http://www.gnu.org/copyleft/gpl.html
// @match        https://www.co-optimus.com/games.php?system=28*
// @match        https://www.co-optimus.com/*
// @include      https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js
// @include      https://cdn.jsdelivr.net/npm/tablesorter@2.31.3/dist/js/jquery.tablesorter.combined.min.js
// @include      https://cdn.jsdelivr.net/npm/tablesorter-pagercontrols@1.4.1/dist/js/jquery.tablesorter.pager.appendcontrols.english.min.js
// @updateURL    https://bitbucket.org/antonolsson91/cooptimus-enhanced-ux/raw/HEAD/userscript.user.js

// @grant        unsafeWindow
// ==/UserScript==
let ceu = {
    results: [],
    decodeEntities: function(encodedString) {
        var textArea = document.createElement('textarea');
        textArea.innerHTML = encodedString;
        return textArea.value;
    },
    rawgKey: null,
    conversionTable: {
        "DKK": {
            ratio: 6.07,
            symbol: "kr",
        },
        "USD": {
            ratio: 1,
            symbol: "$",
        },
        "PL": {
            ratio: 0.2,
            symbol: "złoty",
        },
        "SEK": {
            ratio: 8.2,
            symbol: "kr",
        },
    },
    loader: function(){
        return $(`<img alt="" class="ajaxLoader num${Math.random(500)}" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBzdGFuZGFsb25lPSJubyI/Pgo8IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4iICJodHRwOi8vd3d3LnczLm9yZy9HcmFwaGljcy9TVkcvMS4xL0RURC9zdmcxMS5kdGQiPgo8c3ZnIHdpZHRoPSI0MHB4IiBoZWlnaHQ9IjQwcHgiIHZpZXdCb3g9IjAgMCA0MCA0MCIgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4bWw6c3BhY2U9InByZXNlcnZlIiBzdHlsZT0iZmlsbC1ydWxlOmV2ZW5vZGQ7Y2xpcC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjEuNDE0MjE7IiB4PSIwcHgiIHk9IjBweCI+CiAgICA8ZGVmcz4KICAgICAgICA8c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWwogICAgICAgICAgICBALXdlYmtpdC1rZXlmcmFtZXMgc3BpbiB7CiAgICAgICAgICAgICAgZnJvbSB7CiAgICAgICAgICAgICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDBkZWcpCiAgICAgICAgICAgICAgfQogICAgICAgICAgICAgIHRvIHsKICAgICAgICAgICAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoLTM1OWRlZykKICAgICAgICAgICAgICB9CiAgICAgICAgICAgIH0KICAgICAgICAgICAgQGtleWZyYW1lcyBzcGluIHsKICAgICAgICAgICAgICBmcm9tIHsKICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpCiAgICAgICAgICAgICAgfQogICAgICAgICAgICAgIHRvIHsKICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogcm90YXRlKC0zNTlkZWcpCiAgICAgICAgICAgICAgfQogICAgICAgICAgICB9CiAgICAgICAgICAgIHN2ZyB7CiAgICAgICAgICAgICAgICAtd2Via2l0LXRyYW5zZm9ybS1vcmlnaW46IDUwJSA1MCU7CiAgICAgICAgICAgICAgICAtd2Via2l0LWFuaW1hdGlvbjogc3BpbiAxLjVzIGxpbmVhciBpbmZpbml0ZTsKICAgICAgICAgICAgICAgIC13ZWJraXQtYmFja2ZhY2UtdmlzaWJpbGl0eTogaGlkZGVuOwogICAgICAgICAgICAgICAgYW5pbWF0aW9uOiBzcGluIDEuNXMgbGluZWFyIGluZmluaXRlOwogICAgICAgICAgICB9CiAgICAgICAgXV0+PC9zdHlsZT4KICAgIDwvZGVmcz4KICAgIDxnIGlkPSJvdXRlciI+CiAgICAgICAgPGc+CiAgICAgICAgICAgIDxwYXRoIGQ9Ik0yMCwwQzIyLjIwNTgsMCAyMy45OTM5LDEuNzg4MTMgMjMuOTkzOSwzLjk5MzlDMjMuOTkzOSw2LjE5OTY4IDIyLjIwNTgsNy45ODc4MSAyMCw3Ljk4NzgxQzE3Ljc5NDIsNy45ODc4MSAxNi4wMDYxLDYuMTk5NjggMTYuMDA2MSwzLjk5MzlDMTYuMDA2MSwxLjc4ODEzIDE3Ljc5NDIsMCAyMCwwWiIgc3R5bGU9ImZpbGw6YmxhY2s7Ii8+CiAgICAgICAgPC9nPgogICAgICAgIDxnPgogICAgICAgICAgICA8cGF0aCBkPSJNNS44NTc4Niw1Ljg1Nzg2QzcuNDE3NTgsNC4yOTgxNSA5Ljk0NjM4LDQuMjk4MTUgMTEuNTA2MSw1Ljg1Nzg2QzEzLjA2NTgsNy40MTc1OCAxMy4wNjU4LDkuOTQ2MzggMTEuNTA2MSwxMS41MDYxQzkuOTQ2MzgsMTMuMDY1OCA3LjQxNzU4LDEzLjA2NTggNS44NTc4NiwxMS41MDYxQzQuMjk4MTUsOS45NDYzOCA0LjI5ODE1LDcuNDE3NTggNS44NTc4Niw1Ljg1Nzg2WiIgc3R5bGU9ImZpbGw6cmdiKDIxMCwyMTAsMjEwKTsiLz4KICAgICAgICA8L2c+CiAgICAgICAgPGc+CiAgICAgICAgICAgIDxwYXRoIGQ9Ik0yMCwzMi4wMTIyQzIyLjIwNTgsMzIuMDEyMiAyMy45OTM5LDMzLjgwMDMgMjMuOTkzOSwzNi4wMDYxQzIzLjk5MzksMzguMjExOSAyMi4yMDU4LDQwIDIwLDQwQzE3Ljc5NDIsNDAgMTYuMDA2MSwzOC4yMTE5IDE2LjAwNjEsMzYuMDA2MUMxNi4wMDYxLDMzLjgwMDMgMTcuNzk0MiwzMi4wMTIyIDIwLDMyLjAxMjJaIiBzdHlsZT0iZmlsbDpyZ2IoMTMwLDEzMCwxMzApOyIvPgogICAgICAgIDwvZz4KICAgICAgICA8Zz4KICAgICAgICAgICAgPHBhdGggZD0iTTI4LjQ5MzksMjguNDkzOUMzMC4wNTM2LDI2LjkzNDIgMzIuNTgyNCwyNi45MzQyIDM0LjE0MjEsMjguNDkzOUMzNS43MDE5LDMwLjA1MzYgMzUuNzAxOSwzMi41ODI0IDM0LjE0MjEsMzQuMTQyMUMzMi41ODI0LDM1LjcwMTkgMzAuMDUzNiwzNS43MDE5IDI4LjQ5MzksMzQuMTQyMUMyNi45MzQyLDMyLjU4MjQgMjYuOTM0MiwzMC4wNTM2IDI4LjQ5MzksMjguNDkzOVoiIHN0eWxlPSJmaWxsOnJnYigxMDEsMTAxLDEwMSk7Ii8+CiAgICAgICAgPC9nPgogICAgICAgIDxnPgogICAgICAgICAgICA8cGF0aCBkPSJNMy45OTM5LDE2LjAwNjFDNi4xOTk2OCwxNi4wMDYxIDcuOTg3ODEsMTcuNzk0MiA3Ljk4NzgxLDIwQzcuOTg3ODEsMjIuMjA1OCA2LjE5OTY4LDIzLjk5MzkgMy45OTM5LDIzLjk5MzlDMS43ODgxMywyMy45OTM5IDAsMjIuMjA1OCAwLDIwQzAsMTcuNzk0MiAxLjc4ODEzLDE2LjAwNjEgMy45OTM5LDE2LjAwNjFaIiBzdHlsZT0iZmlsbDpyZ2IoMTg3LDE4NywxODcpOyIvPgogICAgICAgIDwvZz4KICAgICAgICA8Zz4KICAgICAgICAgICAgPHBhdGggZD0iTTUuODU3ODYsMjguNDkzOUM3LjQxNzU4LDI2LjkzNDIgOS45NDYzOCwyNi45MzQyIDExLjUwNjEsMjguNDkzOUMxMy4wNjU4LDMwLjA1MzYgMTMuMDY1OCwzMi41ODI0IDExLjUwNjEsMzQuMTQyMUM5Ljk0NjM4LDM1LjcwMTkgNy40MTc1OCwzNS43MDE5IDUuODU3ODYsMzQuMTQyMUM0LjI5ODE1LDMyLjU4MjQgNC4yOTgxNSwzMC4wNTM2IDUuODU3ODYsMjguNDkzOVoiIHN0eWxlPSJmaWxsOnJnYigxNjQsMTY0LDE2NCk7Ii8+CiAgICAgICAgPC9nPgogICAgICAgIDxnPgogICAgICAgICAgICA8cGF0aCBkPSJNMzYuMDA2MSwxNi4wMDYxQzM4LjIxMTksMTYuMDA2MSA0MCwxNy43OTQyIDQwLDIwQzQwLDIyLjIwNTggMzguMjExOSwyMy45OTM5IDM2LjAwNjEsMjMuOTkzOUMzMy44MDAzLDIzLjk5MzkgMzIuMDEyMiwyMi4yMDU4IDMyLjAxMjIsMjBDMzIuMDEyMiwxNy43OTQyIDMzLjgwMDMsMTYuMDA2MSAzNi4wMDYxLDE2LjAwNjFaIiBzdHlsZT0iZmlsbDpyZ2IoNzQsNzQsNzQpOyIvPgogICAgICAgIDwvZz4KICAgICAgICA8Zz4KICAgICAgICAgICAgPHBhdGggZD0iTTI4LjQ5MzksNS44NTc4NkMzMC4wNTM2LDQuMjk4MTUgMzIuNTgyNCw0LjI5ODE1IDM0LjE0MjEsNS44NTc4NkMzNS43MDE5LDcuNDE3NTggMzUuNzAxOSw5Ljk0NjM4IDM0LjE0MjEsMTEuNTA2MUMzMi41ODI0LDEzLjA2NTggMzAuMDUzNiwxMy4wNjU4IDI4LjQ5MzksMTEuNTA2MUMyNi45MzQyLDkuOTQ2MzggMjYuOTM0Miw3LjQxNzU4IDI4LjQ5MzksNS44NTc4NloiIHN0eWxlPSJmaWxsOnJnYig1MCw1MCw1MCk7Ii8+CiAgICAgICAgPC9nPgogICAgPC9nPgo8L3N2Zz4K" />`)
    },
    currentCurrency: "SEK",
    currenciesDropdownHTML: function(){
        return Object.keys(this.conversionTable).map((key, index) => `<option ${key == this.currentCurrency ? ' selected ' : ''} value="${key}">${key} (USD rate: ${this.conversionTable[key].ratio})</option>`).join("\n");
    },
    trackingEnabled: true,
    nintendoDomain :`https://www.nintendo.com`,
    setNewCurrency: function(){
        let $ = jQuery;
        let $c = $("#currencyChanger")

        this.currentCurrency = $c.val()
        localStorage.setItem("ceu-currency", this.currentCurrency)
        this.switchPrices()

        console.log($c, this.currentCurrency)
    },
    unsetDone: function(){
        let $ = jQuery;
        $('.result_row[done]').each((i, e) => {
            $(e).removeAttr("done")
        }).promise().done( function(){ } );
    },
    fetchWithCache: {
        CACHE: {},
        MAX_AGE: 60000*60, // 60m
        cachedFetch: function(url, opts) {
            let cached = this.CACHE[url];
            if (cached && Date.now()-cached.time<this.MAX_AGE) return cached;

            let req = fetch(url, opts).then( r => r.json() );
            req.time = Date.now();

            // do fancy stuff

            return (this.CACHE[url] = req);
        }
    },
    addGoogleTag: function(){
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                                                      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                            })(window,document,'script','dataLayer','GTM-WSVNDM3'); console.log("Added Google Tag");
    },
    formatPrice: function(price){
        return Math.round(price * this.conversionTable[this.currentCurrency].ratio);
    },
    switchPrices: function(){
        console.group(`this.switchPrices()`);
        this.clearCeu()
        this.unsetDone()
        ceu.lastLoadedPage = ceu.getPage()

        window.addEventListener('popstate', (event) => {
            console.log("location: " + document.location + ", state: " + JSON.stringify(event.state));
        });
        history.pushState({page: ceu.getPage()}, document.title, ceu.getLocationByShareBtn());
        //history.pushState({page: 2}, "title 2", "?page=2");
        //history.replaceState({page: 3}, "title 3", "?page=3");
        //history.back(); // Logs "location: http://example.com/example.html?page=1, state: {"page":1}"
        //history.back(); // Logs "location: http://example.com/example.html, state: null"
        //history.go(2);  // Logs "location: http://example.com/example.html?page=3, state: {"page":3}"

        let l = $('body .result_row').length, lc = 0, rc = 0;
        console.log({l})


        $('body .result_row').each((i, e) => {
            let $e = $(e)
            let $t = $e.find('>td>strong')
            let slug = $e.attr('title')
            let p = $t.parent()
            let query = $t.text()
            console.group(query)
            console.log($e)


            if($e.attr("done") == "true"){ console.log("-", $e); console.groupEnd(); return; }
            $e.attr("done", true); console.log(".");

            ceu.loader().appendTo(p)

            this.fetchWithCache.cachedFetch(`https://u3b6gr4ua3-dsn.algolia.net/1/indexes/ncom_game_en_us?page=0&hitsPerPage=6&clickAnalytics=true&query=${query}&getRankingInfo=true`, {
                "headers": {
                    "accept": "*/*",
                    "accept-language": "sv-SE,sv;q=0.9,en-US;q=0.8,en;q=0.7,de;q=0.6",
                    "sec-fetch-dest": "empty",
                    "sec-fetch-mode": "cors",
                    "sec-fetch-site": "cross-site",
                    "x-algolia-api-key": "6efbfb0f8f80defc44895018caf77504",
                    "x-algolia-application-id": "U3B6GR4UA3"
                },
                "referrer": "https://www.nintendo.com/games/detail/you-died-but-a-necromancer-revived-you-switch/",
                "referrerPolicy": "no-referrer-when-downgrade",
                "body": null,
                "method": "GET",
                "mode": "cors",
                "credentials": "omit",
                "cache": "force-cache"
            })
                .then(nintendoData => {
                console.log(nintendoData);
                if(nintendoData.hits){
                    for(let result of nintendoData.hits){
                        $t.parent().append(`<a href="${this.nintendoDomain + result.url}" onclick="" class="added-by-ceu" style="margin: 0 4px" target="_blank">N</a>`)
                        const isDiscounted = result.msrp == result.lowestPrice;
                        let discountPercentage = 100 - Math.round(( result.lowestPrice/result.msrp ) * 100)
                        let discountPercentageDisplay = `-${discountPercentage}%`
                        let originalPriceDisplay = `<span title="original price: $${result.msrp}"><strike>${this.formatPrice(result.msrp)}</strike></span>`
                        let price = this.formatPrice(result.lowestPrice)
                        let text = `<div style="display: inline;"><a href="${this.nintendoDomain + result.url}" style="display: inline;" target="_blank" title="${result.title}">
<abbr title="original price: $${result.lowestPrice}">${price} ${this.currentCurrency}</abbr></a>
<div style="display: none">
<br>
<span>${result.msrp}</span>
<br>
<span>${result.priceRange}</span>
</div>
</div>`;

                        $e.append(`
<td data-text="${price}" class="added-by-ceu" style="text-align: right">
`+ ( isDiscounted ? '' : `${originalPriceDisplay} (${discountPercentageDisplay})`)+`
<span title="current price: $${result.lowestPrice}">${price}</span>
</td>`)
                        $e.append(`<td class="added-by-ceu page-${this.getPage()}-price-${i}">${this.conversionTable[this.currentCurrency].symbol}</td>`)
                        //p.append(text)
                        break;
                    }
                } else { console.warn($e, `failed`) }
                p.find('.ajaxLoader').remove()

                lc++;
                if(lc==l){
                    console.log("adding th");
                    $('#results_table thead tr').append(`<th class="added-by-ceu" colspan="2">Prices <select id="currencyChanger" name="currencyChanger" onchange="ceu.setNewCurrency()">${this.currenciesDropdownHTML()}</select></th>`);
                    $('#results_table thead tr').append(`<th class="added-by-ceu">Rating</th>`);
                    $('#results_table thead tr').append(`<th class="added-by-ceu">PT <abbr title="playtime to complete game in hours">?</abbr></th>`);

                    this.waitForEl(`#rawgReady`, () => {
                        console.log("adding tablesorter");
                        this.addTablesorter();
                        this.updateTablesorter();
                    })
                }
            });


            this.fetchWithCache.cachedFetch(`https://api.rawg.io/api/games?key=${this.rawgKey}&search=${slug}`).then(rawgData=>{
                console.group(`rawg result for ${slug}:`, rawgData)
                let rawg = rawgData.results[0]

                $t.parent().append(`<a href="https://rawg.io/games/${rawg.slug}" class="added-by-ceu" onclick="" style="margin: 0 4px" target="_blank">R</a>`)
                this.waitForEl(`.page-${this.getPage()}-price-${i}`, () => {
                    rc++;

                    let metacriticHtml = rawg.metacritic == null ? '' : `<br><img width="16" src="https://www.metacritic.com/images/icons/metacritic-icon.svg">${rawg.metacritic}`;

                    $(`<td class="added-by-ceu" data-text="${(rawg.rating + "").replace(".","")}">
${rawg.rating}<sub>/5</sub>
<br>
Max: ${rawg.rating_top}
<br>
<small>#${rawg.ratings_count}</small>
<br>
<small>Score: ${rawg.score}</small>
${metacriticHtml}
</td>`)
                        .attr('title',JSON.stringify(rawg, false, 2))
                        .appendTo($e);

                    $e.append(`<td class="added-by-ceu">${rawg.playtime}</td>`);
                    console.log(`waiting for rc = ${l} to add #rawgReady`, {rc, l, rawgReadyElement: $("#rawgReady")})
                    if(rc==l || rc==l-1){
                        $("body").append(`<div class="added-by-ceu" id="rawgReady"/>`)
                    }
                })
                console.groupEnd()
            })


            console.groupEnd();
            //return false // only first result row for now
        }).promise().done(() => {
            console.log("done!")
        })

        console.groupEnd()
    },
    resultsTableSelector: `body table#results_table`,
    updateTablesorter: function (){
        $(ceu.resultsTableSelector).trigger('updateAll');
        console.log("updated tablesorter")
    },
    removeTablesorter: function (){
        $(ceu.resultsTableSelector).trigger('destroy');
        console.log("destroyed tablesorter")
    },
    addTablesorter: function () {

        /* Documentation for this tablesorter FORK can be found at
 * http://mottie.github.io/tablesorter/docs/
 */
        $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', 'https://mottie.github.io/tablesorter/css/theme.default.css') );
        $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', 'https://mottie.github.io/tablesorter/css/theme.black-ice.css') );
        $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', 'https://mottie.github.io/tablesorter/addons/pager/jquery.tablesorter.pager.css') );

$(`<div id="pager" class="pager">
<style>
/* pager wrapper, div */
.tablesorter-pager {
  padding: 5px;
}
/* pager wrapper, in thead/tfoot */
td.tablesorter-pager {
  background-color: #e6eeee;
  margin: 0; /* needed for bootstrap .pager gets a 18px bottom margin */
}
/* pager navigation arrows */
.tablesorter-pager img {
  vertical-align: middle;
  margin-right: 2px;
  cursor: pointer;
}

/* pager output text */
.tablesorter-pager .pagedisplay {
  padding: 0 5px 0 5px;
  width: 50px;
  text-align: center;
}

/* pager element reset (needed for bootstrap) */
.tablesorter-pager select {
  margin: 0;
  padding: 0;
}

/*** css used when "updateArrows" option is true ***/
/* the pager itself gets a disabled class when the number of rows is less than the size */
.tablesorter-pager.disabled {
  display: none;
}
/* hide or fade out pager arrows when the first or last row is visible */
.tablesorter-pager .disabled {
  /* visibility: hidden */
  opacity: 0.5;
  filter: alpha(opacity=50);
  cursor: default;
}
</style>
  <form>
    <img src="https://mottie.github.io/tablesorter/addons/pager/icons/first.png" class="first"/>
    <img src="https://mottie.github.io/tablesorter/addons/pager/icons/prev.png" class="prev"/>
    <!-- the "pagedisplay" can be any element, including an input -->
    <span class="pagedisplay" data-pager-output-filtered="{startRow:input} &ndash; {endRow} / {filteredRows} of {totalRows} total rows"></span>
    <img src="https://mottie.github.io/tablesorter/addons/pager/icons/next.png" class="next"/>
    <img src="https://mottie.github.io/tablesorter/addons/pager/icons/last.png" class="last"/>
    <select class="pagesize">
      <option value="10">10</option>
      <option value="25">25</option>
      <option value="50">50</option>
      <option value="75">75</option>
      <option value="100">100</option>
      <option selected value="all">All Rows</option>
    </select>
  </form>
</div>`).appendTo(`#added-by-ceu`);
         var $table = $(this.resultsTableSelector), $pager = $('.pager');
        $table.trigger('destroyPager');
 var pagerOptions = {

    // target the pager markup - see the HTML block below
    container: $pager,

    // use this url format "http:/mydatabase.com?page={page}&size={size}&{sortList:col}"
    ajaxUrl: null,

    // modify the url after all processing has been applied
    customAjaxUrl: function(table, url) { return url; },

    // ajax error callback from $.tablesorter.showError function
    // ajaxError: function( config, xhr, settings, exception ) { return exception; };
    // returning false will abort the error message
    ajaxError: null,

    // add more ajax settings here
    // see http://api.jquery.com/jQuery.ajax/#jQuery-ajax-settings
    ajaxObject: { dataType: 'json' },

    // process ajax so that the data object is returned along with the total number of rows
    ajaxProcessing: null,

    // Set this option to false if your table data is preloaded into the table, but you are still using ajax
    processAjaxOnInit: true,

    // output string - default is '{page}/{totalPages}'
    // possible variables: {size}, {page}, {totalPages}, {filteredPages}, {startRow}, {endRow}, {filteredRows} and {totalRows}
    // also {page:input} & {startRow:input} will add a modifiable input in place of the value
    // In v2.27.7, this can be set as a function
    // output: function(table, pager) { return 'page ' + pager.startRow + ' - ' + pager.endRow; }
    output: function(table, pager) { return `size: ${pager.size}, page: ${pager.page}, totalPages: ${pager.totalPages}, filteredPages: ${pager.filteredPages}, startRow: ${pager.startRow}, endRow: ${pager.endRow}, filteredRows: ${pager.filteredRows} and totalRows: ${pager.totalRows} startRow: ${pager.startRow} startRow:input: {startRow:input}– endRow: ${pager.endRow} / totalRows: ${pager.totalRows} rows`; }, // 'page ' + pager.startRow + ' - ' + pager.endRow

    // apply disabled classname (cssDisabled option) to the pager arrows when the rows
    // are at either extreme is visible; default is true
    updateArrows: true,

    // starting page of the pager (zero based index)
    page: 0,

    // Number of visible rows - default is 10
    size: "all",

    // Save pager page & size if the storage script is loaded (requires $.tablesorter.storage in jquery.tablesorter.widgets.js)
    savePages : true,

    // Saves tablesorter paging to custom key if defined.
    // Key parameter name used by the $.tablesorter.storage function.
    // Useful if you have multiple tables defined
    storageKey:'tablesorter-pager',

    // Reset pager to this page after filtering; set to desired page number (zero-based index),
    // or false to not change page at filter start
    pageReset: 0,

    // if true, the table will remain the same height no matter how many records are displayed. The space is made up by an empty
    // table row set to a height to compensate; default is false
    fixedHeight: true,

    // remove rows from the table to speed up the sort of large tables.
    // setting this to false, only hides the non-visible rows; needed if you plan to add/remove rows with the pager enabled.
    removeRows: false,

    // If true, child rows will be counted towards the pager set size
    countChildRows: false,

    // css class names of pager arrows
    cssNext: '.next', // next page arrow
    cssPrev: '.prev', // previous page arrow
    cssFirst: '.first', // go to first page arrow
    cssLast: '.last', // go to last page arrow
    cssGoto: '.gotoPage', // select dropdown to allow choosing a page

    cssPageDisplay: '.pagedisplay', // location of where the "output" is displayed
    cssPageSize: '.pagesize', // page size selector - select dropdown that sets the "size" option

    // class added to arrows when at the extremes (i.e. prev/first arrows are "disabled" when on the first page)
    cssDisabled: 'disabled', // Note there is no period "." in front of this class name
    cssErrorRow: 'tablesorter-errorRow' // ajax error information row

  };

            $table.tablesorter(
            {
                theme: 'default',
                // initialize zebra striping and filter widgets
                widgets: ["zebra", 'columns', "filter"],

                // headers: { 5: { sorter: false, filter: false } },

                widgetOptions : {

                    // extra css class applied to the table row containing the filters & the inputs within that row
                    filter_cssFilter   : '',

                    // If there are child rows in the table (rows with class name from "cssChildRow" option)
                    // and this option is true and a match is found anywhere in the child row, then it will make that row
                    // visible; default is false
                    filter_childRows   : false,

                    // if true, filters are collapsed initially, but can be revealed by hovering over the grey bar immediately
                    // below the header row. Additionally, tabbing through the document will open the filter row when an input gets focus
                    filter_hideFilters : false,

                    // Set this option to false to make the searches case sensitive
                    filter_ignoreCase  : true,

                    // jQuery selector string of an element used to reset the filters
                    filter_reset : '.reset',

                    // Use the $.tablesorter.storage utility to save the most recent filters
                    filter_saveFilters : true,

                    // Delay in milliseconds before the filter widget starts searching; This option prevents searching for
                    // every character while typing and should make searching large tables faster.
                    filter_searchDelay : 300,

                    // Set this option to true to use the filter to find text from the start of the column
                    // So typing in "a" will find "albert" but not "frank", both have a's; default is false
                    filter_startsWith  : false,

                    // Add select box to 4th column (zero-based index)
                    // each option has an associated function that returns a boolean
                    // function variables:
                    // e = exact text from cell
                    // n = normalized value returned by the column parser
                    // f = search filter input value
                    // i = column index
                    /*
                    filter_functions : {

                        // Add select menu to this column
                        // set the column value to true, and/or add "filter-select" class name to header
                        // '.first-name' : true,

                        // Exact match only
                        1 : function(e, n, f, i, $r, c, data) {
                            return e === f;
                        },

                        // Add these options to the select dropdown (regex example)
                        2 : {
                            "A - D" : function(e, n, f, i, $r, c, data) { return /^[A-D]/.test(e); },
                            "E - H" : function(e, n, f, i, $r, c, data) { return /^[E-H]/.test(e); },
                            "I - L" : function(e, n, f, i, $r, c, data) { return /^[I-L]/.test(e); },
                            "M - P" : function(e, n, f, i, $r, c, data) { return /^[M-P]/.test(e); },
                            "Q - T" : function(e, n, f, i, $r, c, data) { return /^[Q-T]/.test(e); },
                            "U - X" : function(e, n, f, i, $r, c, data) { return /^[U-X]/.test(e); },
                            "Y - Z" : function(e, n, f, i, $r, c, data) { return /^[Y-Z]/.test(e); }
                        },

                        // Add these options to the select dropdown (numerical comparison example)
                        // Note that only the normalized (n) value will contain numerical data
                        // If you use the exact text, you'll need to parse it (parseFloat or parseInt)
                        4 : {
                            "< $10"      : function(e, n, f, i, $r, c, data) { return n < 10; },
                            "$10 - $100" : function(e, n, f, i, $r, c, data) { return n >= 10 && n <=100; },
                            "> $100"     : function(e, n, f, i, $r, c, data) { return n > 100; }
                        }
                    }
*/
                }

            })
            .tablesorterPager(pagerOptions);


        console.log("added tablesorter");
    },
    waitForEl: function(selector, callback) {
        if (jQuery(selector).length) {
            callback();
        } else {
            setTimeout(function() {
                ceu.waitForEl(selector, callback);
            }, 100);
        }
    },
    clearCeu: function(){
        $(".added-by-ceu").remove();
    },
    getPage: function(){
        return parseInt( $('#page').val() )
    },
    getLocationByShareBtn: function(){
        let ref = $("#share-button").attr("ref")
        let decodedRef = decodeURIComponent(ref)
        let thisHref = `${document.location.origin}${decodedRef}`

         return thisHref;
    },
    setFilters: function(){
        oldFilter = window.filter;
        filter = () => {

            //
            //$("[done]").removeAttr('done')

            ceu.newFilter() //oldFilter()

        }
    },
    parseAndSetLastPage: function(msg){
        let steps = 0;
        try{
            if(!msg) throw "msg was empty.";

            $(msg).find(`[onclick*="setPage"]`).each((i, e) => {
                steps++
                let $e = $(e)
                let h = $e.text()
                if(isNaN(parseInt(h))) return;
                ceu.lastPage = parseInt(h)
            })
            if(!ceu.lastPage) throw "ceu.lastPage was not set to a number correctly. steps:"+steps;

        }catch(e){
            console.log(`parseAndSetLastPage failed`, e)
        }
    },
    newFilter: function(){
        var filters=new Array();
        //get active filters that aren't checkboxes
        $('.filter:not(.hidden) :input[type!=checkbox]').each(function() {
            filters.push("" + $(this).attr("name") + "=" + $(this).val());
        });
        //get filters that ARE checkboxes
        $('.filter:not(.hidden) :input[type=checkbox]:checked').each(function() {
            filters.push($(this).attr("name") + "=" + $(this).val());
        });
        var p = $('#page').val();
        var q = filters.join("&");
        //sorting and paging
        q+='&page=' + p;
        q+='&sort=' + $('#sort').val() + '&sortDirection=' + $('#sortDirection').val();
        //$('#search_table').html('<div style="height: 400px; width: 100%; margin: 0 auto; text-align: center;"><img src="http://www.co-optimus.com/images/loading.gif"></div>');

        $.ajax({
            type: "GET",
            url: "/ajax/ajax_games.php",
            data: q,
            success: function(msg){
                if(p == 1){
                    $(msg).find("tbody tr").appendTo($('#search_table tbody'))
                } else {
                    $('#search_table tbody').replaceWith($(msg).find("tbody"))
                }

                ceu.lastAjaxPage = this.url;
                ceu.parseAndSetLastPage(msg)

                ceu.results.push(msg)

                $('body #customsearchtitle')
                    .replaceWith($(msg).find("#customsearchtitle"))
                    .append(`Page: ${p}`);

                $('body .pagination')
                    .replaceWith($(msg).find(".pagination"))

                $("tr.result_row > td:first-child").append($(`<a href="${this.url}" title="source of line item" class=""><span>Src</span></a> `))

                document.title = 'Co-Optimus - ' + $('#customsearchtitle').html();
                ga('send', 'pageview', 'games.php?'+q+'&remotefilter=true');
                gtag('event', 'search', {
                    'event_category': 'Game',
                    'event_label': ''+q+''
                });
                //attach listener
                //gets game info to fill top box
                $("tr.result_row > td:first-child").find('.game_info_opener').remove()
                $("tr.result_row > td:first-child").append($(`<a href="#" class="game_info_opener">Info</a>`).click(function() {
                    $('.result_row').removeClass('selected');
                    $(this).addClass('selected');
                    $.ajax({
                        type: "POST",
                        url: "/ajax/ajax_gameInfo.php",
                        data: "id=" + $(this).parent('.result_row').attr("id"),
                        success: function(msg){
                            $('#game_info').html(msg);
                            $('#game-info-modal').foundation('reveal', 'open');
                        }
                    });
                }));

                let shareBtn = $(msg).find('#share-button');
                console.log(`shareBtn was just loaded by filter() with ref: ${shareBtn.attr('ref')}`)

                //share button listener
                $("#share-button").replaceWith(shareBtn.click(function() {
                    $.ajax({
                        type: "POST",
                        url: "/ajax/ajax_shortenURL.php",
                        data: "url=" + $(this).attr("ref"),
                        success: function(msg){
                            $('#share-link-text').attr('value', msg);
                            $('#email-link').attr('href','mailto:?subject=Co-Op Game List&body=I just built this great Co-Op game list on Co-Optimus: ' + msg);
                            $('#share-modal').foundation('reveal', 'open');
                            //copy js
                            ZeroClipboard.setDefaults( { moviePath: 'http://www.co-optimus.com/images/ZeroClipboard.swf' } );
                            var clip = new ZeroClipboard($('#copy-button'))
                            clip.on( 'complete', function(client, args) {
                                $('#copy-success').fadeIn();
                            });
                        }
                    });
                }));
                //END SHARE

                ceu.waitForEl('.result_row>td>strong', function() {
                    ceu.switchPrices()
                    ceu.updateTablesorter()
                })

            }
        });
    },
    loadMore: function(){
        let nextPageNum = ceu.getPage()+1
        let isLastPage = nextPageNum == ceu.lastPage


        setPage(nextPageNum);
        let loader = ceu.loader()
        loader.insertAfter($('#ceuMoreLoader'))
        loader.replaceWith($(`<span>loaded ${nextPageNum}</span>`).fadeOut(2000).remove())

        if( isLastPage ) $('#ceuMoreLoader').fadeOut()
    },
    addCEUHeader: function(){
        $("body #page-content").prepend(`<div id="added-by-ceu" style="z-index: 999999999; color: blue; background: white; top: 0px; left: 0px; right: 0px;" />`)
        $("#added-by-ceu")
            .append(`<h5><a href="http://bitbucket.org/antonolsson91/cooptimus-enhanced-ux/">CoOpTimus - Enhanced UX</a> <small>by <a href="mailto:eagleenterprises08+cooptimus@gmail.com">Anton Olsson</a></small></h5>`)
            .append(`<a href="#" onclick="ceu.switchPrices(); return false;">get prices</a>`)
            .append(`<span> - </span>`)
            .append(`<a href="#" onclick="ceu.addTablesorter(); return false;">reload table</a>`)
            .append(`<span> - </span>`)
            .append(`<a href="#" onclick="ceu.updateTablesorter(); return false;">update table</a>`)
            .append(`<span> - </span>`)
            .append(`<a href="#" onclick="ceu.removeTablesorter(); return false;">destroy table</a>`)
            .append(`<span> - </span>`)
            .append(`<a href="#" id="ceuMoreLoader" onclick="ceu.loadMore(); return false;">load more</a>`)
            .append(`<span> - </span>`)
            .append(`<div>Disclaimer: The loaded userscript modifies the original website extensively, disable the userscript in Tampermonkey if you're having issues using the website.</div>`)
    },
    main: function(){
        console.group(`main()`);

        let $ = jQuery;
        if(this.trackingEnabled) ceu.addGoogleTag()

        $(document).ready(() => {
            console.log("DOMContentLoaded");


            this.waitForEl('.result_row>td>strong', function() {

                ceu.currentCurrency = ((typeof localStorage.getItem("ceu-currency") == "undefined" || localStorage.getItem("ceu-currency") == null) ? "SEK" : localStorage.getItem("ceu-currency")),


                $("#filter-area").css({ position: 'relative' })
                $('.hide-filter-popup').click();

                $(`[onclick*="setSort"]`).each((i, e) => {
                    let t = $(e).text();
                    $(e).replaceWith(`<span>${t}</span>`)
                })

                ceu.addCEUHeader()

                // Run with: https://www.co-optimus.com/games.php?system=28&playerCount=3&countDirection=at%20least
                if(document.title.includes("Nintendo Switch games")){


                    ceu.switchPrices()
                    //setInterval(switchPrices, 2000)
                    ceu.parseAndSetLastPage($('.pagination'))
                    ceu.setFilters()
                }



            });





        })

        console.groupEnd();
    }
};

(function($) {
    //'use strict';
    console.log("a", jQuery.fn.jquery)
    unsafeWindow.jQuery = jQuery;

    unsafeWindow.jQuery.getScript("https://cdn.jsdelivr.net/npm/tablesorter@2.31.3/dist/js/jquery.tablesorter.combined.min.js", () => {
        unsafeWindow.jQuery.getScript("https://mottie.github.io/tablesorter/addons/pager/jquery.tablesorter.pager.js", () => {
            console.log("a", "yappy")
            unsafeWindow.ceu = ceu;
            console.log(unsafeWindow.ceu)
            unsafeWindow.ceu.main()

        })
    })

})(jQuery);